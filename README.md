# ROCm samples and guides

ROCm samples and guides: HPI samples, libraries and benchmarks

# Code convertion from CUDA do HIP

run `hipconvertinplace-perl.sh` on cuda source and cuda includes.
Attention to preprocessor names, replace NVIDIA names like `__CUDA_API__` with 
`__HPI__`

# Tensorflow

## Tensorflow setup
At this moment we are using the python system as there is no Python Module avaliable
```
module load rocm
pip3 install --user tensorflow-rocm
```

## Tensorflow Benchmark

We present numbers for a preliminar test using default conditions. More information on [cnn_banchmark](https://github.com/tensorflow/benchmarks/tree/master/scripts/tf_cnn_benchmarks)

```
git clone -b cnn_tf_v1.15_compatible http://github.com/tensorflow/benchmarks.git
cd benchmark
python3.6 scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py
```

| CTE-PW9[^1] | CTE-AMD[^2] |
| ------ | ------ |
|  24316.52[^3]  |  14503.66[^3]  |

[^1]: Four Nvidia V100 
[^2]: Two AMD MI50
[^3]: Total images/sec

# Pytorch

Using through singularity. Here we use ROCm 3.5 on the host, and ROCm 3.10 on the guest.  
## Pytorch setup 
User Local Machine
```
singularity  pull docker://rocm/pytorch  
scp pytorch_latest.sif amdlogin1.bsc.es:~/
```

Start singularity
```
module load singularity
singularity run pytorch_latest
```

Download and recompile torch
```
git clone https://github.com/ROCmSoftwarePlatform/pytorch.git
cd pytorch
git submodule update --init --recursive
python3.6 tools/amd_build/build_amd.py
export HCC_AMDGPU_TARGET=gfx906 ! don't seems to work
USE_ROCM=1 MAX_JOBS=32 python3.6 setup.py install --user
```

## Pytorch Testing
```
git clone https://github.com/pytorch/examples.git
cd examples/mnist
python3.6 main.py
```
Train Epoch: 14, Time: 2m36.029s[^2]

## hipify fortran Testing

https://github.com/ROCmSoftwarePlatform/hipfort

Just hipifies CUDA Fortran host calls, Kernels must be write in C, not in Cuda Fortran.
